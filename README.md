# Question
A library of classes which allows you to create questions easily for Console Applications!

## How to use

### VB.NET
```vb
' Clone the object
Dim MyQuestion As Question

' Initialize
MyQuestion = New Question(
1, '  Question number/ID, will be displayed to user
"What is CIL?", ' The Question
"Code Intermedia Language", ' Option A
"Common Interface Locale", ' Option B
"Common Intermediate Language", ' Option C (Correct answer)
"Common Integer Language", ' Option D
Question.QuestionOption.C) ' The correct answer (In the case, C)

' Display the question
MyQuestion.Run()

' Check if the user passed that question
If MyQuestion.Passed Then
    Console.WriteLine("Good job!")
Else
    Console.WriteLine("You could do better next time!")
End If

```

### CSharp
```java
// Clone the object
Question myQuestion;

// Initialize
myQuestion = new Question(
    1, // Question number/ID, will be displayed to user
    "What is CIL?", // The Question
    "Code Intermedia Language", // Option A
    "Common Interface Locale", // Option B
    "Common Intermediate Language", // Option C (Correct answer)
    "Common Integer Language", // Option D
    Question.QuestionOption.C); // The correct answer (In this case, C)

// Display the question
myQuestion.Run();

// Check if the user passed that question
if (myQuestion.passed())
{
    Console.WriteLine("Good Job!");
} 
else {
    Console.WriteLine("You could do better next time!");
}

```
