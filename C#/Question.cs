using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuestionCSharp
{


    class Question
    {

        public enum QuestionOption {
            A,
            B,
            C,
            D
        };

        private readonly int _qid;
        private readonly string _question;
        private readonly string _opt_a;
        private readonly string _opt_b;
        private readonly string _opt_c;
        private readonly string _opt_d;
        private readonly QuestionOption _correct_opt;
        private string _usr_opt;
        private bool _pass;

        public Question(int id, string question, string optionA, string optionB, string optionC, string optionD, QuestionOption correctOption)
        {
            // Initialize the object
            this._qid = id;
            this._question = question;
            this._opt_a = optionA;
            this._opt_b = optionB;
            this._opt_c = optionC;
            this._opt_d = optionD;
            this._correct_opt = correctOption;
            this._pass = false;
        }

        /*
         * Returns the ID
         */
        public int getID()
        {
            return _qid;
        }

        /*
         * Converts String to QuestionOption enum
         */
        private QuestionOption? toOptionEnum(string question){
            // Make Question upper case
            question = question.ToUpper();

            // Check if Questions equals the following
            switch (question){
                case "A":
                    return Question.QuestionOption.A;
                case "B":
                    return Question.QuestionOption.B;
                case "C":
                    return Question.QuestionOption.C;
                case "D":
                    return Question.QuestionOption.D;
                default:
                    return null;
            }
        }

        /*
         * Gets the option string from Question option enum
         */
        public string getOptionString(QuestionOption question)
        {
            switch (question)
            {
                case Question.QuestionOption.A:
                    return _opt_a;
                case Question.QuestionOption.B:
                    return _opt_b;
                case Question.QuestionOption.C:
                    return _opt_c;
                case Question.QuestionOption.D:
                    return _opt_d;
                default:
                    return null;
            }
        }

        /*
         * Returns the main question
         */
        public string getQuestion()
        {
            return _question;
        }

        /*
         * Check if the anser is correct, UserAnswer is the Option (e.g. A, B, C) and is case-insensative
         */
        private bool check(string userAnswer)
        {
            if (toOptionEnum(userAnswer) == _correct_opt)
            {
                return true;
            }
            return false;
        }

        /*
         * Check if he passed this question
         */
        public bool passed()
        {
            return _pass;
        }

        /*
         * Run the question
         */
        public void Run()
        {
            // Wait a little bit
            System.Threading.Thread.Sleep(200);


            // Clear the console
            // Let the student focus only on the lesson, nothing else
            Console.Clear();

            // Sets the colour of the text to grey
            Console.ForegroundColor = ConsoleColor.Gray;


            // Write the question and the possible answers
            Console.WriteLine("Question #" + this.getID() + ": " + this.getQuestion());
            Console.WriteLine("A. " + getOptionString(QuestionOption.A));
            Console.WriteLine("B. " + getOptionString(QuestionOption.B));
            Console.WriteLine("C. " + getOptionString(QuestionOption.C));
            Console.WriteLine("D. " + getOptionString(QuestionOption.D));
            
            // Write the input mark
            Console.Write("> ");

            // If the student answer is correct, he passes this
            if(check(Console.ReadLine()))
            {
                _pass = true;
            }

            // Clear the console again
            Console.Clear();
        }

    }
}
