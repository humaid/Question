
' The MIT License (MIT)
'
' Copyright (c) 2015 Sheikh Humaid AlQassimi
' 
' Permission is hereby granted, free of charge, to any person obtaining a copy
' of this software and associated documentation files (the "Software"), to deal
' in the Software without restriction, including without limitation the rights
' to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
' copies of the Software, and to permit persons to whom the Software is
' furnished to do so, subject to the following conditions:
' 
' The above copyright notice and this permission notice shall be included in
' all copies or substantial portions of the Software.
' 
' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
' IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
' FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
' AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
' LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
' THE SOFTWARE.
Public Class Question

    ' Question option enum
    Enum QuestionOption
        A
        B
        C
        D
    End Enum

    Private ReadOnly _qid As Integer
    Private ReadOnly _question As String
    Private ReadOnly _opt_a As String
    Private ReadOnly _opt_b As String
    Private ReadOnly _opt_c As String
    Private ReadOnly _opt_d As String
    Private ReadOnly _correct_opt As QuestionOption
    Private _usr_opt As String
    Private _pass As Boolean

    Public Sub New(ID As Integer, Question As String, OptionA As String, OptionB As String, OptionC As String, OptionD As String, CorrectOption As QuestionOption)
        ' Initialize the object
        _qid = ID
        _question = Question
        _opt_a = OptionA
        _opt_b = OptionB
        _opt_c = OptionC
        _opt_d = OptionD
        _correct_opt = CorrectOption
        _pass = False
    End Sub

    ' Returns the ID
    Public Function GetID() As Integer
        Return _qid
    End Function

    ' Converts String to QuestionOption enum
    Private Function ToOptionEnum(Question As String) As Nullable(Of QuestionOption)
        ' Make Question upper case
        Question = Question.ToUpper

        ' Check if Questions equals the following
        Select Case Question
            Case "A"
                Return QuestionOption.A
            Case "B"
                Return QuestionOption.B
            Case "C"
                Return QuestionOption.C
            Case "D"
                Return QuestionOption.D
        End Select

        ' Or else return nothing (or null)
        Return Nothing
    End Function

    ' Gets the option string from Question option enum
    Public Function GetOptionString(Question As QuestionOption) As String 
        Select Case Question
            Case QuestionOption.A
                Return _opt_a
            Case QuestionOption.B
                Return _opt_b
            Case QuestionOption.C
                Return _opt_c
            Case QuestionOption.D
                Return _opt_d
        End Select
        Return Nothing
    End Function

    ' Returns the main question
    Public Function GetQuestion() As String
        Return _question
    End Function

    ' Check if the anser is correct, UserAnswer is the Option (e.g. A, B, C) and is case-insensative
    Private Function Check(UserAnswer As String) As Boolean
        If ToOptionEnum(UserAnswer) = _correct_opt Then
            Return True
        End If
        Return False
    End Function

    ' Check if he passed this question
    Public Function Passed() As Boolean
        Return _pass
    End Function

    ' Run the question
    Public Sub Run()
        ' Wait a litte bit
        Threading.Thread.Sleep(200)

        ' Clear the console
        ' Let the student focus only on the lesson, nothing else
        Console.Clear()

        ' Set the colour of the text to grey
        Console.ForegroundColor = ConsoleColor.Gray

        ' Write the question and the possible answers
        Console.WriteLine("Question #" & Me.GetID & ": " & Me.GetQuestion)
        Console.WriteLine("A. " & GetOptionString(QuestionOption.A))
        Console.WriteLine("B. " & GetOptionString(QuestionOption.B))
        Console.WriteLine("C. " & GetOptionString(QuestionOption.C))
        Console.WriteLine("D. " & GetOptionString(QuestionOption.D))

        ' Write the input mark
        Console.Write("> ")

        ' If the student answer is correct, he passes this
        If Check(Console.ReadLine) Then
            _pass = True
        End If

        ' Clear console again
        Console.Clear()
    End Sub
End Class
